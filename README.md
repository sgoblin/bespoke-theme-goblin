# bespoke-theme-goblin

A theme for [Bespoke.js](http://markdalgleish.com/projects/bespoke.js) forked from https://github.com/bespokejs/bespoke-theme-nebula

## Usage

This theme is shipped in a [UMD format](https://github.com/umdjs/umd), meaning that it is available as a CommonJS/AMD module or browser global.

#### Add <link rel="stylesheet" href="https://brick.a.ssl.fastly.net/Source+Sans+Pro:300,700,300i,700i/Source+Serif+Pro:400/Fira+Mono:400"> to your html files in addition to this!

For example, when using CommonJS modules:

```js
var bespoke = require('bespoke'),
  goblin = require('bespoke-theme-goblin');

bespoke.from('#presentation', [
  nebula()
]);
```

When using browser globals:

```js
bespoke.from('#presentation', [
  bespoke.themes.goblin()
]);
```

## Package managers

### npm

```bash
$ npm install bespoke-theme-goblin
```

### Bower

```bash
$ bower install bespoke-theme-goblin
```

## Credits

This theme was built with [generator-bespoketheme](https://github.com/markdalgleish/generator-bespoketheme).

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)
